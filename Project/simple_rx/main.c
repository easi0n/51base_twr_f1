#include "stm32f10x.h"
#include "stm32_eval.h"
#include <stdio.h>
#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
#include "port.h"
#include "bphero_uwb.h"
//#include "lcd_oled.h"

extern int rx_main(void);
extern int tx_main(void);

int main(void)
{
    peripherals_init();
    BPhero_UWB_Message_Init();
    BPhero_UWB_Init();	
  rx_main();
	 //   tx_main();
}
