/*******************************************************************************
* 文件名	  	 : USARTConfig.c 
* 描述	         : USART的底层配置函数
* 移植步骤		 :（1）配置函数（管脚，时钟等）
				  （2）控制的参数（极性，波特率，位数，校验位等）
				  （3）中断函数	
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
#include "stm32f10x.h"
#include "USART.h"

void USART2DMAConfiguration(u8 TxBuffer1,u16 num);
/*******************************************************************************
* 文件名	  	 : ADC1GPIOC_Configuration
* 描述	         : 配置 USART2 Tx Rx
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2PinConfiguration(void)
{	
	GPIO_InitTypeDef GPIO_InitStructure;	
	/* 配置 USART2 Tx (PA.02) as alternate function push-pull */
  	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_2;
  	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
  	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
  	GPIO_Init(GPIOA,&GPIO_InitStructure);

	/* 配置 USART2 Rx (PA.03) as input floating */
  	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_3;
  	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IN_FLOATING;
  	GPIO_Init(GPIOA,&GPIO_InitStructure);
}

/*******************************************************************************
* 文件名	  	 : TIM3NVIC_Configuration
* 描述	         : TIM3中断通道4配置
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void TIM3NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* 配置 DMA通道4的中断，中断优先级别为1，响应级别为2 */
//	NVIC_StructInit(&NVIC_InitStructure);
	NVIC_InitStructure.NVIC_IRQChannel=TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=10;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* 文件名	  	 : USART2ClearCounter
* 描述	         : USART2ClearCounter
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2ClearCounter(void)
{
	TIM_SetCounter(TIM3,0x0000);
}

/*******************************************************************************
* 文件名	  	 : USART2StartCounter
* 描述	         : USART2StartCounter
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2StartCounter(void)
{
//	USART2ClearCounter();
//	TIM_Cmd(TIM3, ENABLE);
}

/*******************************************************************************
* 文件名	  	 : USART2StopCounter
* 描述	         : USART2StopCounter
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2StopCounter(void)
{
//	TIM_Cmd(TIM3, DISABLE); 
}

/*******************************************************************************
* 文件名	  	 : TIM3_IRQHandler
* 描述	         : TIM3_IRQHandler
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
//void TIM3_IRQHandler(void)
//{
//	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==SET)
//	{
//		TIM_ClearFlag(TIM3,TIM_IT_Update);
//		USART2RecvResetBufferIndex();
//	}
//}


unsigned int USART2RecvByte(void)
{
	return(USART_ReceiveData(USART2));
}


/*******************************************************************************
* 文件名	  	 : TIM3_Configuration
* 描述	         : TIM3_Configuration
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void TIM3_Configuration(void)
{
//	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
//	TIM_OCInitTypeDef TIM_OCInitStruct;

//	TIM_DeInit(TIM3);
//	TIM3NVIC_Configuration();
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
//	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
//	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure); 
//	TIM_TimeBaseInitStructure.TIM_Period=9999;
//	TIM_TimeBaseInitStructure.TIM_Prescaler=71;	
//	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1;
//	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;	 
//	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);


//	TIM_OCStructInit(&TIM_OCInitStruct);
//	TIM_OCInitStruct.TIM_OCMode=TIM_OCMode_Timing;
//	TIM_OCInitStruct.TIM_OCPolarity=TIM_OCPolarity_High; 
//	TIM_OCInitStruct.TIM_Pulse=4999;
//	TIM_OC1Init(TIM3,&TIM_OCInitStruct);
//		
//	
//	TIM_SelectInputTrigger(TIM3,TIM_TS_ITR1);
//	TIM_ARRPreloadConfig(TIM3,ENABLE);
//	
//	TIM_Cmd(TIM3, ENABLE); 
//	USART2StopCounter();
}

/*******************************************************************************
* 文件名	  	 : USART2NVIC_Configuration
* 描述	         : USART2DMA中断通道4配置
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* 配置 DMA通道4的中断，中断优先级别为1，响应级别为2 */
	NVIC_InitStructure.NVIC_IRQChannel=USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=3;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
/*******************************************************************************
* 文件名	  	 : USART2配置
* 描述	         : baud:USART2波特率
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2_Configuration(unsigned long baud)
{	
	USART_InitTypeDef USART_InitStructure;
	USART2PinConfiguration();
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART2|RCC_APB2Periph_GPIOA, ENABLE);	
	/* 第1步：打开GPIO和USART部件的时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,  ENABLE);
	/* 配置 USART2 参数：115200波特率，一位停止位，八位数据位，无硬件控制 */
	USART_DeInit(USART2);
	USART_StructInit(&USART_InitStructure);
	USART_InitStructure.USART_BaudRate=baud;
	USART_InitStructure.USART_WordLength=USART_WordLength_8b;
	USART_InitStructure.USART_StopBits=USART_StopBits_1;
	USART_InitStructure.USART_Parity=USART_Parity_No;
	USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
	USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None; 
	USART_Init(USART2,&USART_InitStructure);
#if	DMA_MODE
#else
	USART_ITConfig(USART2,USART_IT_TC,ENABLE);	 
	
#endif
	USART2NVIC_Configuration();
	USART_Cmd(USART2,ENABLE);
	TIM3_Configuration();
}

/*******************************************************************************
* 文件名	  	 : USART2SendByte配置
* 描述	         : temp:USART2发送的数据
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2SendByte(unsigned char temp)
{
	USART_SendData(USART2,temp);
}


/*******************************************************************************
* 文件名	  	 : USART2_IRQHandler
* 描述	         : USART2_IRQHandler（USART2发送）中断函数通道
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2_IRQHandler(void)
{
	static u8 Flag=0;
	
	if(USART_GetFlagStatus(USART2,USART_FLAG_RXNE)==SET)
	{
	    USART_ClearFlag(USART2,USART_FLAG_RXNE);//TCIE,TE,RE
		if(Flag)
		{			
			USART2RecvUpdate();
		}
	}
	if(USART_GetFlagStatus(USART2,USART_FLAG_TC)==SET)
	{
	    USART_ClearFlag(USART2,USART_FLAG_TC);//TCIE,TE,RE
		if(Flag)
		{
			USART2SendUpdate();
		}
	}
	Flag=1;
}
/*******************************************************************************
* 文件名	  	 : USART2StopSendISR
* 描述	         : 停止发送中断
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2StopSendISR(void)
{
	USART_ITConfig(USART2,USART_IT_TC,DISABLE);	  	
}
/*******************************************************************************
* 文件名	  	 : USART2StartSendISR
* 描述	         : 开启发送中断
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2StartSendISR(void)
{
	USART_ITConfig(USART2,USART_IT_TC,ENABLE);		 
}
/*******************************************************************************
* 文件名	  	 : USART2StopRecvISR
* 描述	         : 停止接收中断
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2StopRecvISR(void)
{
	USART_ITConfig(USART2,USART_IT_RXNE,DISABLE);
}
/*******************************************************************************
* 文件名	  	 : USART2StartRecvISR
* 描述	         : 开启接收中断
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2StartRecvISR(void)
{
	USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
}
/*******************************************************************************
* 文件名	  	 : USART2DMAConfiguration
* 描述	         : 开启接收中断
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/

#define USART2_DR_Base  0x40013804

void USART2DMAConfig(u8 TxBuffer1,u16 num)
{
    DMA_InitTypeDef DMA_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
   // DMA1 Channel4 (triggered by USART2 Tx event) Config 
    DMA_DeInit(DMA1_Channel4);
    DMA_InitStructure.DMA_PeripheralBaseAddr = USART2_DR_Base;
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)TxBuffer1;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = num;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel4, &DMA_InitStructure);
	
	DMA_ITConfig(DMA1_Channel4,DMA_IT_TC,ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	DMA_Cmd(DMA1_Channel4, ENABLE);
	USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
	USART_Cmd(USART2,ENABLE);
}		   

/*******************************************************************************
* 文件名	  	 : DMA1_Channel4_IRQHandler
* 描述	         : DMA1_Channel4_IRQHandler（USART2发送）DMA函数通道
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
/*void DMA1_Channel4_IRQHandler(void)
{
	if(DMA_GetITStatus(DMA1_IT_TC4)==SET)
	{
	    DMA_ClearFlag(DMA1_IT_TC4);//TCIE,TE,RE			
		USART2DMAUpdate();
	}
}	*/  




	 


