/*******************************************************************************
* 文件名	  	 : USART2.c
* 描述	         : USART的驱动函数
* 移植步骤		 : 中间层函数
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
#include "stm32f10x.h"
#include "OSQMem.h"
#include "USART.h"

#define USART2_SEND_MAX_Q	  	(OS_MEM_USART2_BLK-4)	//发送内存块内的最大空间
#define USART2_SEND_MAX_BOX		20	   					//发送内存块的最大数量

unsigned char USART2SendQBoxHost=0;						//内存块头指针							
unsigned char USART2SendQBoxTail=0;						//内存块尾指针
unsigned int  USART2SendQFree=USART2_SEND_MAX_BOX;   
unsigned char USART2SendOVF=0; 							//USART2发送任务块溢出标志
unsigned char USART2RunningFlag=0;
typedef struct{
unsigned char Num;
unsigned char *Index;
}USART2SendTcb;
USART2SendTcb USART2SendTCB[USART2_SEND_MAX_BOX];

#define USART2_RECV_MAX_Q	  	32						//内存块内的最大空间
unsigned char USART2QRecvBuffer[USART2_RECV_MAX_Q];		//接收内存块	
unsigned char USART2RecvOVF=0; 							//USART2接收任务块溢出标志  
unsigned int Recv2Index=0x00;
unsigned int Recv2Count=0x00;
unsigned char USART2RecvFlag=0;

extern u8 OSUSART2MemQ[OS_MEM_USART2_MAX];  			//空白内存块
extern OSMEMTcb* OSQUSART2Index;

void USART2DMAUpdate(void);
//错误定义
#define ERR_NO_SPACE	0xff

/*******************************************************************************
* 文件名	  	 : USART2SendUpdate
* 描述	         : 检查结构体里面有没有数据还未发送完毕，若没有发送，则继续发送，
				   若发送完毕，退出
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2SendUpdate(void)
{
	static unsigned char count=0;
	
	if(USART2SendQFree==USART2_SEND_MAX_BOX){return;}
	USART2StopSendISR();
	//如果现在的内存块的数据还没有发送完毕，启动发送，Num减一
	if((USART2SendTCB[USART2SendQBoxTail].Num)&&(USART2SendQBoxTail!=USART2SendQBoxHost))
	{
		USART2SendTCB[USART2SendQBoxTail].Num--;
		USART2SendByte(*(USART2SendTCB[USART2SendQBoxTail].Index+count));
		count++;
	}
	//一个发送块已经发送完毕了 ，USART2SendQFree++,尾指针加一。指向下一个发送块
	else if(USART2SendQBoxTail!=USART2SendQBoxHost)
	{		
		OSMemDelete(OSQUSART2Index,USART2SendTCB[USART2SendQBoxTail].Index);
		if(++USART2SendQBoxTail>=USART2_SEND_MAX_BOX)USART2SendQBoxTail=0;
		if(++USART2SendQFree>=USART2_SEND_MAX_BOX)USART2SendQFree=USART2_SEND_MAX_BOX;
		count=0;
		//USART2SendQBoxTail等于USART2SendQBoxTail的时候就标志这发送结束了，可以直接退出
		if((USART2SendQBoxTail!=USART2SendQBoxHost))
//		if((USART2SendTCB[USART2SendQBoxTail].Num)&&(USART2SendQBoxTail!=USART2SendQBoxHost))
		{
			USART2SendTCB[USART2SendQBoxTail].Num--;
			USART2SendByte(*(USART2SendTCB[USART2SendQBoxTail].Index+count));
			count++;
		}
		else
		{	
		//USART2SendQBoxTail等于USART2SendQBoxTail的时候就标志这发送结束了，可以直接退出
			USART2RunningFlag=0;
			USART2SendQFree=USART2_SEND_MAX_BOX;
			count=0;
		}	
	}
	//由于头指针一直是指向空的发送块的，所以USART2SendQBoxTail等于USART2SendQBoxTail
	//的时候就标志这发送结束了，可以直接退出
	else
	{
		USART2RunningFlag=0;
		USART2SendQFree=USART2_SEND_MAX_BOX;
		count=0;
	}
	USART2StartSendISR();	
}
/*******************************************************************************
* 文件名	  	 : USART2WriteDataToBuffer
* 描述	         : 检查发送缓冲区的大小，若空间足够，将待发送的数据放入到发送缓冲
				   区中去,并且启动发送
* 输入           : buffer待发送的数据的指针，count待发送的数据的数量
* 输出           : 无
* 返回           : 若正确放入到发送缓冲区中去了，就返回0x00	 ，否则返回0x01
*******************************************************************************/
unsigned char USART2WriteDataToBuffer(unsigned char *buffer,unsigned char count)
{
	unsigned char i=count;
	u8 err;
	/*此处可以加入信号灯或者关闭中断*/
	if(count==0)return 0;
	USART2StopSendISR();
	/*计算放入count个数据需要多少个内存块*/
	if(count%USART2_SEND_MAX_Q)count=count/USART2_SEND_MAX_Q+1;
	else count=count/USART2_SEND_MAX_Q;
	/*需要count个数据块*/
	/*如果内存不足，直接返回*/		 
	if(USART2SendQFree<count){USART2StartSendISR();return ERR_NO_SPACE;}
	//首先申请内存块，USART2SendQBoxHost在下一个内存申请后才加一
	USART2SendTCB[USART2SendQBoxHost].Index=(u8 *)OSMemGet(OSQUSART2Index,&err);
	if(USART2SendQBoxHost>=USART2_SEND_MAX_BOX)USART2SendQBoxHost=0;	
	count=0;
	while(i!='\0')										 
	{
		*(USART2SendTCB[USART2SendQBoxHost].Index+count)=*buffer;
		count++;
		if(count>=USART2_SEND_MAX_Q)
		{
			USART2SendTCB[USART2SendQBoxHost].Num=USART2_SEND_MAX_Q;
			//需要一个新的内存块存放接下来的数据，所以更新USART2SendQBoxHost
			if(++USART2SendQBoxHost>=USART2_SEND_MAX_BOX)USART2SendQBoxHost=0;
			//需要一个新的内存块存放接下来的数据	
			USART2SendTCB[USART2SendQBoxHost].Index=(u8 *)OSMemGet(OSQUSART2Index,&err);
			//空的发送任务块减一 			
			USART2SendQFree--;
			count=0;
		}
		buffer++;
		i--;
	}
	//此处是尚未整块存完的数据，它们也要存放在一个新的内存块里
	if(count!=0)
	{
		USART2SendTCB[USART2SendQBoxHost].Num=count; 
		USART2SendQFree--;
		if(++USART2SendQBoxHost>=USART2_SEND_MAX_BOX)USART2SendQBoxHost=0;
	}
	//如果是第一次，就启动发送，如果是已经启动就没有这个必要了
	if(USART2RunningFlag==0)
	{
#if	  	DMA_MODE
		USART2DMAConfig(USART2SendTCB[USART2SendQBoxTail].Index,USART2SendTCB[USART2SendQBoxTail].Num);
#else	
		USART2SendUpdate();
#endif		
		USART2RunningFlag=1;
	}
	/*此处可以开启信号灯或者打开中断*/
	USART2StartSendISR();
	return 0x00;
}
/*******************************************************************************
* 文件名	  	 : USART2DispFun
* 描述	         : 检查发送缓冲区的大小，若空间足够，将待发送的数据放入到发送缓冲
				   区中去,并且启动发送,与USART2WriteDataToBuffer不同的是，启动发送
				   函数世不需要指定文件大小的，这就给调用提供了方便.
* 输入           : buffer待发送的数据的指针
* 输出           : 无
* 返回           : 若正确放入到发送缓冲区中去了，就返回0x00	 ，否则返回0x01
*******************************************************************************/
unsigned char USART2DispFun(unsigned char *buffer)
{
	unsigned long count=0;
	while(buffer[count]!='\0')count++;
	return(USART2WriteDataToBuffer(buffer,count));
}


/*******************************************************************************
* 文件名	  	 : USART2RecvResetBufferIndex
* 描述	         : 当发生超时中断的时候，将接收的指针归零，并且关闭检查超时的时钟
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2RecvResetBufferIndex(void)
{	
//	static u8 PinYinIndex[6];
//	static u8 Count=0xff;

//	USART2WriteDataToBuffer(USART2QRecvBuffer,Recv2Index);
	USART1WriteDataToBuffer(USART2QRecvBuffer,Recv2Index);
	USART2StopCounter();
	Recv2Index=0;
/*	if(Count==0xff){Count=0;return;}

	if(((*USART2QRecvBuffer>='a')&&(*USART2QRecvBuffer<='z'))||((*USART2QRecvBuffer>='A')&&(*USART2QRecvBuffer<='Z')))
	{
		if(Count<6)
		{
			PinYinIndex[Count++]=*USART2QRecvBuffer;
		}
		else 
		{
			USART2DispFun("\n\r");
			USART2DispFun((char *)PYSearch(PinYinIndex));
			USART2DispFun("\n\r");
			while(Count!=0)PinYinIndex[Count--]=0;
			
		}		  
	}	
	else if(*USART2QRecvBuffer=='\r')
	{
		if(Count!=0)
		{
			USART2DispFun("\n\r");
			USART2DispFun((char *)PYSearch(PinYinIndex));
			USART2DispFun("\n\r");
			while(Count!=0)PinYinIndex[Count--]=0;
		}
	}
	else 
	{
		USART2DispFun("\n\r错误的拼音代码，请重新输入!\n\r");
		while(Count!=0)PinYinIndex[Count--]=0;	
	}			 */
	
	
}
//unsigned char test1;	
/*******************************************************************************
* 文件名	  	 : USART2RecvFun
* 描述	         : 当接收到完整的一帧数据以后的处理函数
* 输入           : ptr接收到的数据帧的头指针，接收到的数据帧的数据个数
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2RecvFun(unsigned char *ptr,unsigned int count)
{
//	USART2WriteDataToBuffer(ptr,count);
		USART1WriteDataToBuffer(ptr,count);
}
/*******************************************************************************
* 文件名	  	 : USART2RecvUpdate
* 描述	         : 处理接收到一个数据
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/	
void USART2RecvUpdate(void)
{
	if(Recv2Index<Recv2Count)
	{
		USART2QRecvBuffer[Recv2Index++]=(unsigned char)USART2RecvByte();
		//知道接收到指定数量的数据
		if(Recv2Index>=Recv2Count)
		{
			Recv2Index=0;
			USART2StopCounter();
			USART2RecvFun(USART2QRecvBuffer,Recv2Count);
		}
		if(USART2RecvFlag==1)USART2StartCounter();	
		else USART2StopCounter();	
	}	
}
/*******************************************************************************
* 文件名	  	 : USART2RecvData
* 描述	         : 当接收到完整的一帧数据以后的处理函数
* 输入           : count：要接收到的一帧数据数据的个数，flag：1开启超时中断
				   0关闭超时中断
* 输出           : 无
* 返回           : 无
*******************************************************************************/	
unsigned char USART2RecvData(unsigned int count,unsigned char flag)
{
	if(count!=0)
	{
		Recv2Count=count;
		USART2RecvFlag=flag;
		if(flag==1)USART2StartRecvISR();
	}
	else if(count>USART2_RECV_MAX_Q) 
		return ERR_NO_SPACE;
	return 0x00;
}
/*******************************************************************************
* 文件名	  	 : USART2DMAUpdate.c
* 描述	         : USART_DMA的驱动函数
* 移植步骤		 : 中间层函数
* 输入           : 无
* 输出           : 无
* 返回           : 无
*******************************************************************************/
void USART2DMAUpdate(void)
{
	if(USART2SendQBoxTail!=USART2SendQBoxHost)
	{
		OSMemDelete(OSQUSART2Index,USART2SendTCB[USART2SendQBoxTail].Index);
		if(++USART2SendQBoxTail>=USART2_SEND_MAX_BOX)USART2SendQBoxTail=0;
		if(++USART2SendQFree>=USART2_SEND_MAX_BOX)USART2SendQFree=USART2_SEND_MAX_BOX;
		if(USART2SendQBoxTail!=USART2SendQBoxHost)
		{
			USART2DMAConfig(USART2SendTCB[USART2SendQBoxTail].Index,USART2SendTCB[USART2SendQBoxTail].Num);	
		}
		else USART2RunningFlag=0;	
	}
	else 
	{		
		OSMemDelete(OSQUSART2Index,USART2SendTCB[USART2SendQBoxTail].Index);
		if(++USART2SendQBoxTail>=USART2_SEND_MAX_BOX)USART2SendQBoxTail=0;
		if(++USART2SendQFree>=USART2_SEND_MAX_BOX)USART2SendQFree=USART2_SEND_MAX_BOX;
		USART2RunningFlag=0;
	}	
}
