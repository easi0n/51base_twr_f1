/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

#include "stm32f10x.h"
#include "stm32_eval.h"
#include <stdio.h>
#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
#include "port.h"
#include "lcd_oled.h"
#include "frame_header.h"
#include "bp_filter.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"

static void Handle_TimeStamp(void);
#define MAX_ANTHOR_NODE 40
struct distance_struct
{
    double rx_distance;
    struct time_timestamp tx_node;
    struct time_timestamp rx_node;
    bool present;
    int count;
} bphero_distance[MAX_ANTHOR_NODE];

typedef signed long long int64;
typedef unsigned long long uint64;
static uint64 poll_rx_ts;
static uint64 resp_tx_ts;
static uint64 final_rx_ts;

static uint64 poll_tx_ts;
static uint64 resp_rx_ts;
static uint64 final_tx_ts;

/* UWB microsecond (uus) to device time unit (dtu, around 15.65 ps) conversion factor.
 * 1 uus = 512 / 499.2 ? and 1 ? = 499.2 * 128 dtu. */
#define UUS_TO_DWT_TIME 65536

/* Delay between frames, in UWB microseconds. See NOTE 4 below. */
/* This is the delay from Frame RX timestamp to TX reply timestamp used for calculating/setting the DW1000's delayed TX function. This includes the
 * frame length of approximately 2.46 ms with above configuration. */
#define POLL_RX_TO_RESP_TX_DLY_UUS 2600
/* This is the delay from the end of the frame transmission to the enable of the receiver, as programmed for the DW1000's wait for response feature. */
#define RESP_TX_TO_FINAL_RX_DLY_UUS 500
/* Receive final timeout. See NOTE 5 below. */
#define FINAL_RX_TIMEOUT_UUS 3300


/* Delay between frames, in UWB microseconds. See NOTE 4 below. */
/* This is the delay from the end of the frame transmission to the enable of the receiver, as programmed for the DW1000's wait for response feature. */
#define POLL_TX_TO_RESP_RX_DLY_UUS 150
/* This is the delay from Frame RX timestamp to TX reply timestamp used for calculating/setting the DW1000's delayed TX function. This includes the
 * frame length of approximately 2.66 ms with above configuration. */
#define RESP_RX_TO_FINAL_TX_DLY_UUS 2800 //2700 will fail
/* Receive response timeout. See NOTE 5 below. */
#define RESP_RX_TIMEOUT_UUS 2700

static srd_msg_dsss *msg_f;
static float uwb_rssi = 0;
static int freq_count = 0;
static double tof;
static double distance;
static double sum_distance = 0;
static int distance_count = 0;
extern dwt_config_t config;
/* Private functions ---------------------------------------------------------*/
void Simple_Rx_Callback()
{
    uint32 status_reg = 0,i=0;
    uint32 poll_tx_ts, resp_rx_ts, final_tx_ts;
    uint32 poll_rx_ts_32, resp_tx_ts_32, final_rx_ts_32;
    double Ra, Rb, Da, Db;
    int64 tof_dtu;
    char dist_str[16] = {0};
    for (i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    /* Activate reception immediately. See NOTE 2 below. */
    dwt_enableframefilter(DWT_FF_RSVD_EN);//disable recevie
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);

    if (status_reg & SYS_STATUS_RXFCG)//good message
    {
        /* A frame has been received, copy it to our local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
        if (frame_len <= FRAME_LEN_MAX)
        {
            dwt_readrxdata(rx_buffer, frame_len, 0);
            msg_f = (srd_msg_dsss*)rx_buffer;
            //copy source address as dest address
            msg_f_send.destAddr[0] = msg_f->sourceAddr[0];
            msg_f_send.destAddr[1] = msg_f->sourceAddr[1];
            //copy source seqNum
            msg_f_send.seqNum = msg_f->seqNum;

            switch(msg_f->messageData[0])
            {
                case 'D'://distance
                    led_on(LED_ALL);
                    uwb_rssi = dwGetReceivePower();
                    freq_count++;
                    break;

                case 'P':
                    /* Retrieve poll reception timestamp. */
                    poll_rx_ts = get_rx_timestamp_u64();
                    /* Set expected delay and timeout for final message reception. */
//                                  dwt_setrxaftertxdelay(RESP_TX_TO_FINAL_RX_DLY_UUS);
//                                  dwt_setrxtimeout(FINAL_RX_TIMEOUT_UUS);
                    msg_f_send.messageData[0]='A';//Poll ack message
                    //后面修改这个数据长度
                    dwt_writetxdata(psduLength, (uint8 *)&msg_f_send, 0) ; // write the frame data
                    dwt_writetxfctrl(psduLength, 0);
                    dwt_starttx(DWT_START_TX_IMMEDIATE);
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };
                    break;

                case 'F':
                    //  OLED_ShowString(0, 2,"Rec Final");
                    /* Retrieve response transmission and final reception timestamps. */
                    resp_tx_ts = get_tx_timestamp_u64();
                    final_rx_ts = get_rx_timestamp_u64();

                    /* Get timestamps embedded in the final message. */
                    final_msg_get_ts(&msg_f->messageData[FINAL_MSG_POLL_TX_TS_IDX], &poll_tx_ts);
                    final_msg_get_ts(&msg_f->messageData[FINAL_MSG_RESP_RX_TS_IDX], &resp_rx_ts);
                    final_msg_get_ts(&msg_f->messageData[FINAL_MSG_FINAL_TX_TS_IDX], &final_tx_ts);

                    /* Compute time of flight. 32-bit subtractions give correct answers even if clock has wrapped. See NOTE 10 below. */
                    poll_rx_ts_32 = (uint32)poll_rx_ts;
                    resp_tx_ts_32 = (uint32)resp_tx_ts;
                    final_rx_ts_32 = (uint32)final_rx_ts;
                    Ra = (double)(resp_rx_ts - poll_tx_ts);
                    Rb = (double)(final_rx_ts_32 - resp_tx_ts_32);
                    Da = (double)(final_tx_ts - resp_rx_ts);
                    Db = (double)(resp_tx_ts_32 - poll_rx_ts_32);
                    tof_dtu = (int64)((Ra * Rb - Da * Db) / (Ra + Rb + Da + Db));

                    tof = tof_dtu * DWT_TIME_UNITS;
                    distance = tof * SPEED_OF_LIGHT;
                    //distance = distance - dwt_getrangebias(config.chan,(float)distance, config.prf);//距离减去矫正系数
                    msg_f_send.messageData[0]='d';//Poll ack message
                    int temp = (int)(distance*100);
                    distance_count++;
                    sum_distance =sum_distance+distance;
                    {
                        sprintf(dist_str, "an0:%3.2fm", distance);
                        OLED_ShowString(0, 4,dist_str);
                        distance_count= 0;
                        sum_distance = 0;
                    }
//                    msg_f_send.messageData[1] = temp/100;
//                    //                        // a=x;  //自动类型转换，取整数部分
//                    msg_f_send.messageData[2] = temp%100;  //乘100后对100取余，得到2位小数点后数字
//                    //后面修改这个数据长度
//                    dwt_writetxdata(psduLength, (uint8 *)&msg_f_send, 0) ; // write the frame data
//                    dwt_writetxfctrl(psduLength, 0);
//                    dwt_starttx(DWT_START_TX_IMMEDIATE);
//                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
//                    { };
                    break;

                default:
                    break;
            }
        }
        //enable recive again
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_setrxtimeout(0);
        dwt_rxenable(0);
    }
    else
    {
        //clear error flag
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
        //enable recive again
        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_rxenable(0);
    }
}

extern int time4_overflow;
int rx_main(void)
{
    char lcd_char[100]= {'\0'};
    //Enable RX
    dwt_setaddress16(0x0033);
    dwt_setrxtimeout(0);
    dwt_enableframefilter(DWT_FF_DATA_EN);
    dwt_rxenable(0);
    //Set Rx callback
    bphero_setcallbacks(Simple_Rx_Callback);
    while (1)
    {
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */
/**
  * @}
  */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
